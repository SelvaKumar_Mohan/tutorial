import 'package:flutter/material.dart';
import 'package:tutorial/core/container/topic_container.dart';
import 'package:tutorial/core/router/route_generator.dart';
import 'package:tutorial/core/router/topic_router.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initDependencies();
  runApp(MyApp());
}

initDependencies() async {
  await topicContainer.createDependencies();
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Tutorials',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: TopicRouter.topicsListRoute,
      onGenerateRoute: RouteGenerator.getInstance().generateRoute,
    );
  }
}
