import 'package:sqflite/sqflite.dart';

abstract class TableCreator {
  Future<void> createTable(Database database);
}