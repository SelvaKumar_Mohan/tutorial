import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'table_creator.dart';

class TutorialsDatabase {
  static const String DB_NAME = "tutorials_database.db";

  static Future<Database> init(List<TableCreator> tableCreators) async {
    return openDatabase(
      join(await getDatabasesPath(), DB_NAME),
      onCreate: (db, version) async {
        tableCreators.forEach((element) async {
          await element.createTable(db);
        });
      },
      version: 1,
    );
  }
}
