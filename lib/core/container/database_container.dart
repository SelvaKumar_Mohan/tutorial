import 'package:sqflite/sqflite.dart';
import 'package:tutorial/core/database/tutorials_database.dart';
import 'package:tutorial/topic/repository/database/notes_table_creator.dart';

class DatabaseContainer {
  Database _database;

  DatabaseContainer._();

  Future<Database> createDatabase() {
    if (_database == null) {
      final tableCreators = [
        NotesTableCreator()
      ];
      return TutorialsDatabase.init(tableCreators);
    } else {
      return Future.value(_database);
    }
  }
}

final DatabaseContainer databaseContainer = DatabaseContainer._();