import 'package:tutorial/core/container/database_container.dart';
import 'package:tutorial/topic/di/topic_dependency_provider.dart';

class TopicContainer {
  TopicDependencyProvider topicDependencyProvider;

  TopicContainer._();

  createDependencies() async {
    final database = await databaseContainer.createDatabase();
    topicDependencyProvider = TopicDependencyProvider(database);
  }
}

TopicContainer topicContainer = TopicContainer._();
