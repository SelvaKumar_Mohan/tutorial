import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

abstract class BaseRouter {
  Iterable<String> get routes;

  Widget generateDestinationWidget(
      RouteSettings settings, BuildContext context);
}
