import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:tutorial/core/router/base_router.dart';
import 'package:tutorial/core/router/topic_router.dart';

class RouteGenerator {
  static RouteGenerator _instance;

  final Iterable<BaseRouter> _routers = [
    topicRouter,
  ];

  RouteGenerator._();

  factory RouteGenerator.getInstance() {
    return _instance ??= RouteGenerator._();
  }

  Route<dynamic> generateRoute(RouteSettings settings) {
    return MaterialPageRoute<dynamic>(
        builder: (context) => _generateDestinationWidget(settings, context),
        settings: RouteSettings(name: settings.name));
  }

  Widget _generateDestinationWidget(
      RouteSettings settings, BuildContext context) {
    for (final router in _routers) {
      if (router.routes.contains(settings.name)) {
        return router.generateDestinationWidget(settings, context);
      }
    }
    return _routeError(settings);
  }

  Widget _routeError(RouteSettings settings) {
    return Scaffold(
      body: Center(
        child: Text('No route defined for ${settings.name}'),
      ),
    );
  }
}
