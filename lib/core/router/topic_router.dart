import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/navigator.dart';
import 'package:tutorial/core/container/topic_container.dart';
import 'package:tutorial/core/router/base_router.dart';
import 'package:tutorial/topic/domain/model/topic.dart';
import 'package:tutorial/topic/presentation/topic_list.dart';
import 'package:tutorial/topic/presentation/topic_screen.dart';

class TopicRouter implements BaseRouter {
  static const String topicsListRoute = "/";
  static const String topicDetailsRoute = "/topic_details_screen";

  TopicRouter._();

  @override
  Widget generateDestinationWidget(
      RouteSettings settings, BuildContext context) {
    switch (settings.name) {
      case topicsListRoute:
        return TopicList(topicContainer.topicDependencyProvider.topicPresenter,
            (context, topic) {
          Navigator.pushNamed(context, topicDetailsRoute, arguments: topic);
        });
      case topicDetailsRoute:
        return TopicScreen(settings.arguments as Topic,
            topicContainer.topicDependencyProvider.topicPresenter);
      default:
        throw Exception("named route ${settings.name} doesn't exists");
    }
  }

  @override
  Iterable<String> get routes => [topicsListRoute, topicDetailsRoute];
}

TopicRouter topicRouter = TopicRouter._();
