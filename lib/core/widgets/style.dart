import 'package:flutter/material.dart';

TextStyle textStyleHeader1() => TextStyle(color: Colors.black, fontSize: 25);
TextStyle textStyleHeader2() => TextStyle(color: Colors.black, fontSize: 18);
TextStyle textStyleSubHeader1() => TextStyle(fontSize: 15);
TextStyle textStyleSubHeader2() => TextStyle(fontSize: 16);
TextStyle textStyleError() => TextStyle(fontSize: 18, color: Colors.red);