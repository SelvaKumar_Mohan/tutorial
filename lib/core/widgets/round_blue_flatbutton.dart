import 'package:flutter/material.dart';

class RoundBlueFlatButton extends StatelessWidget {
  final String title;
  final VoidCallback onPressedAction;

  const RoundBlueFlatButton(this.title, this.onPressedAction, {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: TextButton.styleFrom(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          padding: EdgeInsets.all(8)),
      onPressed: onPressedAction,
      child: Text(
        title,
      ),
    );
  }
}
