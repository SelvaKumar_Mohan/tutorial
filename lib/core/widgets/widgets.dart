import 'package:flutter/widgets.dart';

Widget horizontalSpace() => Container(
      width: 16.0,
    );

Widget verticalSpace() => Container(
      height: 8.0,
    );
