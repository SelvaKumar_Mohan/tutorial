import 'package:flutter/cupertino.dart';
import 'package:tutorial/core/widgets/style.dart';

Text textHeader1(String text) => Text(
      text,
      style: textStyleHeader1(),
    );

Text textSubHeader1(String text) => Text(
      text,
      style: textStyleSubHeader1(),
    );

Text textHeader2(String text) => Text(
  text,
  style: textStyleHeader2(),
);

Text textSubHeader2(String text) => Text(
  text,
  style: textStyleSubHeader2(),
);

Text textError(String error) => Text(error, style: textStyleError(),);