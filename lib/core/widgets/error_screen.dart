import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:tutorial/core/widgets/round_blue_flatbutton.dart';

class InScreenError extends StatelessWidget {
  final String _title;
  final String _message;
  final Widget _errorIcon;
  final Function() _retry;

  const InScreenError(
      {String title = "Error",
      String message = "Unknown error occurred",
      Widget errorIcon,
      Function() retry})
      : _title = title,
        _message = message,
        _errorIcon = errorIcon,
        _retry = retry;

  @override
  Widget build(BuildContext context) {
    final widgets = _getMandatoryWidgets(context);
    if (_retry != null) {
      widgets.addAll([
        const SizedBox(height: 8),
        Center(child: RoundBlueFlatButton("retry", _retry))
      ]);
    }
    return Padding(
        padding: EdgeInsets.all(8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: widgets,
        ));
  }

  List<Widget> _getMandatoryWidgets(BuildContext context) {
    return <Widget>[
      _errorIcon ?? Icon(Icons.error),
      const SizedBox(height: 8),
      Center(
          child: Text(
        _title,
        textAlign: TextAlign.center,
      )),
      const SizedBox(height: 8),
      Center(
          child: Text(
        _message,
        textAlign: TextAlign.center,
      )),
    ];
  }
}
