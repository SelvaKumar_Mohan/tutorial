abstract class Result<S, F> {
  const Result._();

  B fold<B>(B Function(S s) ifSuccess, B Function(F f) ifFailure);

  bool isSuccess() => fold((_) => true, (_) => false);

  S asSuccess() => (this as Success<S, F>).value;

  F asFailure() => (this as Failure<S, F>).value;

  bool isFailure() => fold((_) => false, (_) => true);

  Result<S2, F> map<S2>(S2 Function(S s) f) =>
      fold((S s) => success(f(s)), failure);

  Result<S2, F> flatMap<S2>(Result<S2, F> Function(S s) f) => fold(f, failure);
}

class Failure<S, F> extends Result<S, F> {
  final F _f;

  const Failure._(this._f) : super._();

  F get value => _f;

  @override
  B fold<B>(B Function(S s) ifSuccess, B Function(F f) ifFailure) =>
      ifFailure(_f);

  @override
  bool operator ==(dynamic other) => other is Failure && other._f == _f;

  @override
  int get hashCode => _f.hashCode;
}

class Success<S, F> extends Result<S, F> {
  final S _s;

  const Success._(this._s) : super._();

  S get value => _s;

  @override
  B fold<B>(B Function(S s) ifSuccess, B Function(F f) ifFailure) =>
      ifSuccess(_s);

  @override
  bool operator ==(dynamic other) => other is Success && other._s == _s;

  @override
  int get hashCode => _s.hashCode;
}

Result<S, F> failure<S, F>(F f) => Failure._(f);

Result<S, F> success<S, F>(S s) => Success._(s);
