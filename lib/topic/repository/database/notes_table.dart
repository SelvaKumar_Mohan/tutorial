const TABLE_NOTES = "NotesTable";

const COLUMN_ID = "id";
const COLUMN_TOPIC_ID = "topicId";
const COLUMN_NOTE = "note";
const COLUMN_TIMESTAMP = "timestamp";
