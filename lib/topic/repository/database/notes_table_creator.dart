import 'package:sqflite_common/sqlite_api.dart';
import 'package:tutorial/core/database/table_creator.dart';
import 'package:tutorial/topic/repository/database/notes_table.dart';

class NotesTableCreator implements TableCreator {

  @override
  Future<void> createTable(Database database) {
    return database.execute("CREATE TABLE IF NOT EXISTS $TABLE_NOTES($COLUMN_ID TEXT PRIMARY KEY, $COLUMN_TOPIC_ID TEXT, $COLUMN_TIMESTAMP TEXT, $COLUMN_NOTE TEXT)");
  }

}