import 'package:rxdart/rxdart.dart';
import 'package:sqflite/sqflite.dart';
import 'package:tutorial/topic/domain/model/note.dart';
import 'package:tutorial/topic/domain/model/topic.dart';
import 'package:tutorial/topic/domain/repository/topic_repository.dart';
import 'package:tutorial/result.dart';
import 'package:tutorial/topic/repository/database/notes_table.dart';
import 'package:tutorial/topic/repository/mapper/topic_data_mapper_facade.dart';

class TopicRepositoryImpl implements TopicRepository {
  final Database _topicDatabase;
  final TopicDataMapperFacade _mapperFacade;
  final Map<String, BehaviorSubject> observableTopics =
      {}; // Assuming only one topic can be active at a time.

  TopicRepositoryImpl(this._topicDatabase, this._mapperFacade);

  @override
  Future<Result<void, String>> addNote(String topicId, Note note) async {
    try {
      await _topicDatabase.insert(
          TABLE_NOTES, _mapperFacade.toNoteDtoMapper(topicId, note));
      BehaviorSubject<Iterable<Note>> notesForTopic = observableTopics[topicId];
      if (notesForTopic == null) {
        notesForTopic = BehaviorSubject<Iterable<Note>>();
        observableTopics.putIfAbsent(topicId, () => notesForTopic);
      }
      _getAllNotesForTopic(topicId).then((value) => notesForTopic.add(value));
    } catch (e) {
      return Future.value(failure(
          "UNKNOWN_ERROR")); // Not worriyin much about error handling now. Handle it properly.
    }
  }

  @override
  Stream<Iterable<Note>> observeNotesForTopic(String topicId) {
    BehaviorSubject<Iterable<Note>> notesForTopic = observableTopics[topicId];
    if (notesForTopic == null) {
      notesForTopic = BehaviorSubject<Iterable<Note>>();
      observableTopics.putIfAbsent(topicId, () => notesForTopic);
    }
    if (notesForTopic.value == null)
      _getAllNotesForTopic(topicId).then((value) => notesForTopic.add(value));

    return notesForTopic.stream;
  }

  Future<Iterable<Note>> _getAllNotesForTopic(String topicId) async {
    List<Map<String, dynamic>> noteDTos = await _topicDatabase.query(
      TABLE_NOTES,
      where: "$COLUMN_TOPIC_ID = ?",
      whereArgs: <String>[topicId],
      orderBy: "$COLUMN_TIMESTAMP DESC",
    );

    return _mapperFacade.toNotesMapper(noteDTos);
  }

  @override
  Future<Result<Iterable<Topic>, String>> getAllTopics() {
    return Future.delayed(const Duration(seconds: 2), () {
      var topics = <Topic>[];
      for (var i = 0; i < 5; i++) {
        topics.add(
            Topic("$i", [], "Topic $i", "Short description about Topic $i"));
      }
      return success(topics);
    });
  }
}
