import 'package:tutorial/topic/domain/model/note.dart';
import 'package:tutorial/topic/repository/database/notes_table.dart';

typedef MapToNoteDomain = Note Function(Map<String, dynamic>);
typedef MapToNotesDomain = List<Note> Function(Iterable<Map<String, dynamic>>);

Note toNoteDomain(Map<String, dynamic> noteDto) {
  return Note(int.parse(noteDto[COLUMN_TIMESTAMP] as String), noteDto[COLUMN_NOTE] as String);
}

List<Note> toNotes(Iterable<Map<String, dynamic>> notesDto) {
  return notesDto.map((noteDto) => toNoteDomain(noteDto)).toList();
}