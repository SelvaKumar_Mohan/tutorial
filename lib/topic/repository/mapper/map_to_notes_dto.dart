import 'package:tutorial/topic/domain/model/note.dart';
import 'package:tutorial/topic/repository/database/notes_table.dart';
import 'package:uuid/uuid.dart';

typedef MapToNoteDto = Map<String, dynamic> Function(String topicId, Note note);

Map<String, dynamic> toNoteDto(String topicId, Note note) => <String, dynamic>{
  COLUMN_ID : Uuid().v4(),
  COLUMN_TOPIC_ID: topicId,
  COLUMN_NOTE: note.note,
  COLUMN_TIMESTAMP: note.timeStampInMilliSec
};