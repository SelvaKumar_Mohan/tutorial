import 'package:tutorial/topic/repository/mapper/map_to_note_domain.dart';
import 'package:tutorial/topic/repository/mapper/map_to_notes_dto.dart';

class TopicDataMapperFacade {
  final MapToNotesDomain toNotesMapper;
  final MapToNoteDomain toNoteMapper;
  final MapToNoteDto toNoteDtoMapper;

  TopicDataMapperFacade(
      this.toNoteDtoMapper, this.toNoteMapper, this.toNotesMapper);
}
