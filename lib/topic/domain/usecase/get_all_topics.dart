import 'package:tutorial/result.dart';
import 'package:tutorial/topic/domain/model/topic.dart';
import 'package:tutorial/topic/domain/repository/topic_repository.dart';

class GetAllTopicsUseCase {
  TopicRepository _topicRepository;

  GetAllTopicsUseCase(this._topicRepository);

  Future<Result<Iterable<Topic>, String>> call() {
    return _topicRepository.getAllTopics();
  }
}