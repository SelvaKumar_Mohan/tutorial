import 'package:tutorial/topic/domain/model/note.dart';
import 'package:tutorial/topic/domain/repository/topic_repository.dart';
import 'package:tutorial/result.dart';

class AddNoteToTopicUseCase {
  TopicRepository _topicRepository;

  AddNoteToTopicUseCase(this._topicRepository);

  Future<Result<void, String>> call(String topicId, Note noteToAdd) {
    return _topicRepository.addNote(topicId, noteToAdd);
  }
}