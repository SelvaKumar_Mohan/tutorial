import 'package:tutorial/topic/domain/model/note.dart';
import 'package:tutorial/topic/domain/repository/topic_repository.dart';

class GetAllNotesForTopicUseCase {
  TopicRepository _topicRepository;

  GetAllNotesForTopicUseCase(this._topicRepository);

  Stream<Iterable<Note>> call(String topicId) {
    return _topicRepository.observeNotesForTopic(topicId);
  }
}
