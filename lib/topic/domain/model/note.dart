class Note {
  int _timeStampInMilliSec;
  String _note;

  Note(this._timeStampInMilliSec, this._note);
  
  int get timeStampInMilliSec => _timeStampInMilliSec;

  String get note => _note;
  
  @override
  int get hashCode {
    return _timeStampInMilliSec.hashCode ^ _note.hashCode;
  }

  @override
  bool operator ==(Object other) {
    if (other.runtimeType == this.runtimeType &&
        other is Note &&
        this._timeStampInMilliSec == other._timeStampInMilliSec &&
        this._note == other._note) return true;
    return false;
  }
}
