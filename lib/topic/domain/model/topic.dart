import 'package:tutorial/topic/domain/model/note.dart';

class Topic {
  String _id;
  String _title;
  String _description;
  List<Note> _notes;

  Topic(this._id, this._notes, this._title, this._description);

  String get id => _id;

  String get title => _title;

  String get description => _description;

  List<Note> get notes => _notes;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Topic &&
          runtimeType == other.runtimeType &&
          _id == other._id &&
          _title == other._title &&
          _description == other._description &&
          _notes == other._notes;

  @override
  int get hashCode =>
      _id.hashCode ^ _title.hashCode ^ _description.hashCode ^ _notes.hashCode;
}
