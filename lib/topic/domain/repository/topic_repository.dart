import 'package:tutorial/topic/domain/model/note.dart';
import 'package:tutorial/result.dart';
import 'package:tutorial/topic/domain/model/topic.dart';

abstract class TopicRepository {
  Stream<Iterable<Note>> observeNotesForTopic(String topicId);
  Future<Result<void, String>> addNote(String topicId, Note note);
  Future<Result<Iterable<Topic>, String>> getAllTopics();
}
