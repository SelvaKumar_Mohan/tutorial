import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:tutorial/core/widgets/text.dart';
import 'package:tutorial/core/widgets/widgets.dart';
import 'package:tutorial/topic/domain/model/note.dart';
import 'package:tutorial/topic/domain/model/topic.dart';
import 'package:tutorial/topic/presentation/custom_video_player.dart';
import 'package:tutorial/topic/presentation/topic_presenter.dart';
import 'package:video_player/video_player.dart';

class TopicScreen extends StatefulWidget {
  final Topic _topic;
  final TopicPresenter _topicPresenter;

  TopicScreen(this._topic, this._topicPresenter);

  @override
  _TopicScreenState createState() => _TopicScreenState();
}

class _TopicScreenState extends State<TopicScreen> {
  VideoPlayerController _videoPlayerController;

  @override
  void initState() {
    _videoPlayerController =
        VideoPlayerController.asset("assets/demo_topic.mp4");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(widget._topic.title)),
      body: _portrait(),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          Duration duration = await _videoPlayerController.position;
          print('Add notes clicked ${duration.inSeconds}');
          widget._topicPresenter.addNote(
              widget._topic.id,
              duration.inSeconds,
              "To set the custom font manually, open your activity file and insert this into the onCreate() method");
        },
        child: Icon(Icons.add),
      ),
    );
  }

  Widget _portrait() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _getVideoWidget(),
        Container(
          height: 10,
        ),
        _getCuePointsWidget(),
      ],
    );
  }

  Widget _getVideoWidget() {
    return Flexible(
        flex: 1,
        fit: FlexFit.tight,
        child: CustomVideoPlayer(_videoPlayerController));
  }

  Widget _getCuePointsWidget() {
    return StreamBuilder(
      stream: widget._topicPresenter.observeNotesStream(widget._topic.id),
      builder: (_, snapshot) {
        if (snapshot.hasData) {
          List<Note> notes = snapshot.data as List<Note>;
          return Flexible(
            flex: 2,
            fit: FlexFit.tight,
            child: ListView.separated(
                itemBuilder: (context, index) => _buildNoteWidget(notes[index]),
                separatorBuilder: (context, index) => Divider(
                      color: Colors.black12,
                    ),
                itemCount: notes.length),
          );
        }

        return Container();
      },
    );
  }

  Widget _buildNoteWidget(Note note) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        textHeader2("${note.timeStampInMilliSec}"),
        horizontalSpace(),
        Expanded(child: textSubHeader2(note.note)),
      ],
    );
  }
}
