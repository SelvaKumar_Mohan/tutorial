import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:rxdart/rxdart.dart';
import 'package:tutorial/core/widgets/error_screen.dart';
import 'package:tutorial/core/widgets/text.dart';
import 'package:tutorial/topic/domain/model/topic.dart';
import 'package:tutorial/topic/presentation/topic_presenter.dart';
import 'package:tutorial/topic/presentation/topics_list_view_model.dart';

class TopicList extends StatefulWidget {
  final TopicPresenter _topicPresenter;
  final Function(BuildContext, Topic topic) _toTopicDetails;

  TopicList(this._topicPresenter, this._toTopicDetails);

  @override
  _TopicListState createState() => _TopicListState();
}

class _TopicListState extends State<TopicList> {
  final _compositeSubscription = CompositeSubscription();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Tutorials'),
      ),
      body: StreamBuilder<TopicsListViewModel>(
        stream: widget._topicPresenter.topicsListViewModel,
        builder: (_, snapshot) {
          if (snapshot.hasData) {
            final viewModel = snapshot.data;
            if (viewModel is LoadingTopicsList) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else if (viewModel is LoadedTopicsList) {
              return _buildTopicsListWidget(viewModel.topics);
            } else if (viewModel is ErrorLoadingTopicsList) {
              return InScreenError(
                message: viewModel.errorMessage,
                retry: widget._topicPresenter.getAllTopics,
              );
            } else {
              return InScreenError(
                retry: widget._topicPresenter.getAllTopics,
              );
            }
          } else {
            return InScreenError(
              retry: widget._topicPresenter.getAllTopics,
            );
          }
        },
      ),
    );
  }

  @override
  void initState() {
    widget._topicPresenter.getAllTopics();
    super.initState();
  }

  @override
  void dispose() {
    _compositeSubscription.clear();
    super.dispose();
  }

  Widget _buildTopicsListWidget(Iterable<Topic> topics) {
    return ListView.separated(
        itemBuilder: (context, index) =>
            _buildTopicsListItemWidget(topics.elementAt(index)),
        separatorBuilder: (context, index) => Divider(
              color: Colors.blueGrey,
            ),
        itemCount: topics.length);
  }

  Widget _buildTopicsListItemWidget(Topic topic) {
    return InkWell(
      onTap: () {
        widget._toTopicDetails(context, topic);
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 16.0, top: 8.0, right: 16.0),
            child: textHeader1("${topic.title}"),
          ),
          Container(
            height: 8,
          ),
          Padding(
            padding: const EdgeInsets.only(
                left: 16.0, top: 4.0, right: 16.0, bottom: 8.0),
            child: textSubHeader1("${topic.description}"),
          ),
        ],
      ),
    );
  }
}
