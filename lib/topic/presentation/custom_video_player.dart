import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:tutorial/core/widgets/text.dart';
import 'package:video_player/video_player.dart';

class CustomVideoPlayer extends StatefulWidget {
  final VideoPlayerController _controller;
  final Function() onPlay;
  final Function() onPause;

  CustomVideoPlayer(this._controller, {this.onPlay, this.onPause});

  @override
  _CustomVideoPlayerState createState() => _CustomVideoPlayerState();
}

class _CustomVideoPlayerState extends State<CustomVideoPlayer> {

  ChewieController _chewieController;

  @override
  void initState() {
    _chewieController =
        ChewieController(videoPlayerController: widget._controller,
            autoInitialize: true,
            autoPlay: true,
            looping: false,
            errorBuilder: _videoErrorDialog);
    super.initState();
  }


  @override
  void dispose() {
    _chewieController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Chewie(controller: _chewieController);
  }

  Widget _videoErrorDialog(BuildContext context, String errorMessage) {
    return Center(
      child: textError(errorMessage),
    );
  }
}
