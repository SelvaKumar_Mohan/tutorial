import 'package:tutorial/topic/domain/model/topic.dart';

abstract class TopicsListViewModel {
  TopicsListViewModel._();

  factory TopicsListViewModel.loading() => LoadingTopicsList._();

  factory TopicsListViewModel.loaded(Iterable<Topic> topics) => LoadedTopicsList._(topics);

  factory TopicsListViewModel.error(String errorMessage) => ErrorLoadingTopicsList._(errorMessage);
}

class LoadingTopicsList extends TopicsListViewModel {
  LoadingTopicsList._() : super._();

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is LoadingTopicsList && runtimeType == other.runtimeType;

  @override
  int get hashCode => 0;
}

class LoadedTopicsList extends TopicsListViewModel {
  final Iterable<Topic> topics;

  LoadedTopicsList._(this.topics) : super._();

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is LoadedTopicsList &&
          runtimeType == other.runtimeType &&
          topics == other.topics;

  @override
  int get hashCode => topics.hashCode;
}

class ErrorLoadingTopicsList extends TopicsListViewModel {
  final String errorMessage;

  ErrorLoadingTopicsList._(this.errorMessage) : super._();

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ErrorLoadingTopicsList &&
          runtimeType == other.runtimeType &&
          errorMessage == other.errorMessage;

  @override
  int get hashCode => errorMessage.hashCode;
}