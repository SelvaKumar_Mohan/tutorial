import 'package:rxdart/rxdart.dart';
import 'package:tutorial/topic/domain/model/note.dart';
import 'package:tutorial/topic/domain/usecase/add_note_to_topic.dart';
import 'package:tutorial/topic/domain/usecase/get_all_notes_for_topic.dart';
import 'package:tutorial/topic/domain/usecase/get_all_topics.dart';
import 'package:tutorial/topic/presentation/topics_list_view_model.dart';

class TopicPresenter {
  final GetAllNotesForTopicUseCase _getAllNotesForTopicUseCase;
  final AddNoteToTopicUseCase _addNoteToTopicUseCase;
  final GetAllTopicsUseCase _getAllTopicsUseCase;
  final _topicsListViewModel = BehaviorSubject<TopicsListViewModel>();

  Stream<TopicsListViewModel> get topicsListViewModel =>
      _topicsListViewModel.stream;

  TopicPresenter(this._getAllNotesForTopicUseCase, this._addNoteToTopicUseCase,
      this._getAllTopicsUseCase);

  void addNote(String topicId, int timeStamp, String note) {
    _addNoteToTopicUseCase(topicId, Note(timeStamp, note));
  }

  Stream<Iterable<Note>> observeNotesStream(String topicId) {
    return _getAllNotesForTopicUseCase(topicId);
  }

  String _formattedTime(int milliSec) {
    DateTime dateTime = DateTime.fromMicrosecondsSinceEpoch(milliSec);
  }

  void getAllTopics() {
    _topicsListViewModel.add(TopicsListViewModel.loading());
    _getAllTopicsUseCase().then((result) {
      result.fold(
          (s) => _topicsListViewModel.add(TopicsListViewModel.loaded(s)),
          (f) => _topicsListViewModel.add(TopicsListViewModel.error(f)));
    });
  }
}
