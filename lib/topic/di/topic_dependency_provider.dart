import 'package:sqflite/sqflite.dart';
import 'package:tutorial/topic/domain/usecase/add_note_to_topic.dart';
import 'package:tutorial/topic/domain/usecase/get_all_notes_for_topic.dart';
import 'package:tutorial/topic/domain/usecase/get_all_topics.dart';
import 'package:tutorial/topic/presentation/topic_presenter.dart';
import 'package:tutorial/topic/repository/mapper/map_to_note_domain.dart';
import 'package:tutorial/topic/repository/mapper/map_to_notes_dto.dart';
import 'package:tutorial/topic/repository/mapper/topic_data_mapper_facade.dart';
import 'package:tutorial/topic/repository/topic_repository_impl.dart';

class TopicDependencyProvider {
  AddNoteToTopicUseCase _addNoteToTopicUseCase;
  GetAllNotesForTopicUseCase _getAllNotesForTopicUseCase;
  GetAllTopicsUseCase _getAllTopicsUseCase;
  TopicPresenter _topicPresenter;

  TopicDependencyProvider(Database database) {
    final topicRepository = TopicRepositoryImpl(database, TopicDataMapperFacade(
        toNoteDto, toNoteDomain, toNotes));
    _addNoteToTopicUseCase = AddNoteToTopicUseCase(topicRepository);
    _getAllNotesForTopicUseCase = GetAllNotesForTopicUseCase(topicRepository);
    _getAllTopicsUseCase = GetAllTopicsUseCase(topicRepository);
    _topicPresenter = TopicPresenter(_getAllNotesForTopicUseCase, _addNoteToTopicUseCase, _getAllTopicsUseCase);
  }

  GetAllNotesForTopicUseCase get getAllNotesForTopicUseCase =>
      _getAllNotesForTopicUseCase;

  AddNoteToTopicUseCase get addNoteToTopicUseCase => _addNoteToTopicUseCase;

  TopicPresenter get topicPresenter => _topicPresenter;
}